require 'rails_helper'

RSpec.describe ApplicationHelper, :type => :helper do
  describe "#full_title" do

    it "include the base title" do
      assign(:base_title, "Shopping Cart App")
      expect(helper.full_title).to eq("Shopping Cart App")
    end

    it "include the page title" do
      assign(:base_title, "Shopping Cart App")
      expect(helper.full_title("Home")).to eq("Home | Shopping Cart App")
    end
  end
end